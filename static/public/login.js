function login() {
	var usename = document.getElementById("input-username").value;
	var password = document.getElementById("input-password").value;
	var salt = sha3_256(usename + "studibars-ac");
	var hashed_pw = sha3_256(password + salt);

	document.getElementById("input-hashed-password").value = hashed_pw;
	document.getElementById("form-login").submit();
}