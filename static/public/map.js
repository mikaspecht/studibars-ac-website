var map = L.map('map').setView([50.78, 6.0758], 14);
var BarIcon = L.Icon.extend({
	options: {
		iconSize: [64, 64], // size of the icon
		iconAnchor: [32, 32], // point of the icon which will correspond to marker's location
		popupAnchor: [0, -32] // point from which the popup should open relative to the iconAnchor
	}
});

L.tileLayer('https://tile.openstreetmap.org/{z}/{x}/{y}.png', {
	attribution: '&copy; <a href="https://www.openstreetmap.org/copyright">OpenStreetMap</a> contributors'
}).addTo(map);

L.marker([50.7748424, 6.0680286], { icon: new BarIcon({ iconUrl: 'logo_triangel_red_small.png' }) }).addTo(map)
	.bindPopup('<h3>Triangel</h3><p> Montags ab 21 Uhr</p><p>Studentenwohnheim am Weißenberg<br>Am Weissenberg 16-18</p>');
L.marker([50.7781679, 6.0670530], { icon: new BarIcon({ iconUrl: 'logo_faho_small.png' }) }).addTo(map)
	.bindPopup('<h3>FAHO Bierkeller</h3><p>Mittwochs ab 20 Uhr</p><p>FAHO Studentenwohnheim<br>Hainbuchenstraße 6 </p>');
L.marker([50.7786993, 6.0851869], { icon: new BarIcon({ iconUrl: 'logo_seilbar_small.png' }) }).addTo(map)
	.bindPopup('<h3>SEILgraben Bar</h3><p>1./3./5. Donnerstag im Monat ab 21 Uhr</p><p>Wohnheim Seilgraben<br>Seilgraben 34-36</p>');
L.marker([50.7882288, 6.0708622], { icon: new BarIcon({ iconUrl: 'logo_motorbar_small.png' }) }).addTo(map)
	.bindPopup('<h3>Club Motorbar</h3><p>Mittwochs ab 20:30 Uhr</p><p>Studentenwohnheim Otto-Petersen-Haus (Türme)<br>Rütscher Straße 155</p>');
L.marker([50.7760765, 6.0739856], { icon: new BarIcon({ iconUrl: 'logo_baerenhoele_orange_small.png' }) }).addTo(map)
	.bindPopup('<h3>Bärenhöhle</h3><p>Dienstags ab 21 Uhr</p><p>Studierendenwohnheim B5<br>Bärenstraße 5</p>');