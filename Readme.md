# Studibars Aachen Website
Eine Website gedacht als Hompeage für die Aachener Wohnheimsbars. Fokus soll darauf liegen anstehende Events aller Bars zentral und automatisiert anzukündigen.


Das Design ist WIP und halt typisch Informatiker ohne Designkenntnisse.

Contributers are Welcome!
## (Geplante) Features
- [x] Admin Login
- [x] Anlegen von Events
- [ ] Ändern/löschen von Events
- [ ] Instagram Bot
	- [ ] Posten der Events
	- [ ] Wochenüberblick in der Story
	- [ ] Am Eventtag Event in die Story packen.