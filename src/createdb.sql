CREATE TABLE IF NOT EXISTS users (user_id INTEGER PRIMARY KEY AUTOINCREMENT,username TEXT, hashed_pw TEXT);
CREATE TABLE IF NOT EXISTS bars (bar_id INTEGER PRIMARY KEY AUTOINCREMENT,bar_name TEXT, bar_display_name TEXT, bar_logo_url TEXT, bar_openstreetmap_loc TEXT);
CREATE TABLE IF NOT EXISTS events (event_id INTEGER PRIMARY KEY AUTOINCREMENT,event_name TEXT, event_image_url TEXT, event_description TEXT, event_date DATE);

