use crate::session::{SessionToken, SESSIONS};
use rusqlite::Connection;

use std::time::SystemTime;

#[derive(FromForm)]
pub struct LoginData<'a> {
	username: &'a str,
	hashed_password: &'a str,
}

pub fn create_session(login_data: &LoginData) -> Result<SessionToken, ()> {
	let connection = Connection::open("databas.sqlite").unwrap();
	let statement = "SELECT EXISTS (SELECT * FROM users WHERE username = ? AND hashed_pw = ?)";

	let mut found = false;
	connection
		.prepare(statement)
		.unwrap()
		.query_row(&[login_data.username, login_data.hashed_password], |row| {
			found = 1 == row.get::<usize, i32>(0)?;
			Ok(())
		})
		.unwrap();
	if found {
		unsafe {
			let mut token = SessionToken::new();
			while SESSIONS.contains_key(&token) {
				token = SessionToken::new();
			}
			SESSIONS.insert(token, SystemTime::now());
			println!("New Token: {}", hex::encode(token));
			return Ok(token);
		}
	}
	Err(())
}
