use crate::{
	elements::slideshow::{Slide, Slideshow},
	html::{Html, HtmlElement},
};
use rusqlite::Connection;

pub fn create_index_site(admin: bool) -> Html {
	let connection = Connection::open("databas.sqlite").unwrap();
	let mut statement = connection
		.prepare("SELECT event_description, event_image_url FROM events")
		.unwrap();
	let slides = statement
		.query_map([], |row| {
			Ok(Slide {
				text: Html(row.get(0)?),
				image_uri: row.get(1)?,
			})
		})
		.unwrap()
		.into_iter()
		.map(|slide| slide.unwrap())
		.collect();
	let slideshow = Slideshow { slides };

	Html(format!(
		include_str!("html/index.html"),
		slideshow.to_html()
	))
}
