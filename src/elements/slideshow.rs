use crate::html::{Html, HtmlElement};
pub struct Slide {
	pub text: Html,
	pub image_uri: String,
}

impl HtmlElement for Slide {
	fn to_html(&self) -> Html {
		Html(format!(
			"
			<div class=\"slide-container\">
				<img src=\" {}\" class=\"slide-img\">
				<div class=\"slide-text\">{}</div>
			</div>",
			self.image_uri, self.text
		))
	}
}

pub struct Slideshow {
	pub slides: Vec<Slide>,
}

impl HtmlElement for Slideshow {
	fn to_html(&self) -> Html {
		Html(format!(
			"<div class=\"slideshow-container\">
			{}
			</div>",
			self.slides
				.iter()
				.map(|x| x.to_html().to_string())
				.collect::<Vec<String>>()
				.join("\n")
		))
	}
}
