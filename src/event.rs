use rocket::{fs::TempFile, time::Date};
use rusqlite::Connection;

#[derive(FromForm)]
pub struct EventData<'a> {
	event_name: &'a str,
	event_description: &'a str,
	event_date: Date,
	event_image: TempFile<'a>,
}

pub async fn add_event(event: &mut EventData<'_>) {
	let connection = Connection::open("databas.sqlite").unwrap();
	println!("{}", event.event_date.to_string().as_str());
	let statement =
		"INSERT INTO events (event_name, event_description, event_date) VALUES (?, ?, ?);";
	connection
		.prepare(statement)
		.unwrap()
		.execute([
			event.event_name,
			event.event_description,
			event.event_date.to_string().as_str(),
		])
		.unwrap();
	let id = connection.last_insert_rowid();
	let local_path = format!(
		"/event-img/{}-{}.jpg",
		id,
		event.event_image.name().unwrap()
	);
	let path = format!("static/public{}", local_path);
	event.event_image.move_copy_to(&path).await.unwrap();
	connection
		.execute(
			"UPDATE events SET event_image_url = ? WHERE event_id = ?",
			[local_path, id.to_string()],
		)
		.unwrap();
}
