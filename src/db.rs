use rusqlite::Connection;

pub fn create_datatabase_if_not_exists() {
	let connection = Connection::open("databas.sqlite").unwrap();
	let query = include_str!("createdb.sql");
	connection.execute_batch(query).unwrap();
}
