use std::{collections::HashMap, fmt::Debug, time::SystemTime};

use once_cell::sync::Lazy;
use rand::RngCore;
use rocket::{
	http::Status,
	outcome::Outcome,
	request::{self, FromRequest},
	Request,
};

pub static mut SESSIONS: Lazy<HashMap<SessionToken, SystemTime>> = Lazy::new(|| HashMap::new());

#[derive(PartialEq, Eq, PartialOrd, Ord, Hash, Clone, Copy)]
pub struct SessionToken([u8; 128]);

impl SessionToken {
	pub fn new() -> Self {
		let mut data = [0u8; 128];
		rand::thread_rng().fill_bytes(&mut data);
		SessionToken(data)
	}
}
impl AsRef<[u8]> for SessionToken {
	fn as_ref(&self) -> &[u8] {
		&self.0
	}
}

impl Debug for SessionToken {
	fn fmt(&self, f: &mut std::fmt::Formatter<'_>) -> std::fmt::Result {
		f.write_str(hex::encode(&self.0).as_str())
	}
}

pub struct Session {
	pub token: SessionToken,
}

#[rocket::async_trait]
impl<'a> FromRequest<'a> for Session {
	type Error = &'a str;
	async fn from_request(req: &'a Request<'_>) -> request::Outcome<Self, Self::Error> {
		//get the cookie
		let token = req.cookies().get_private("session-token");
		if token.is_none() {
			// no Session token = Not Logged in
			// We forward so there can be not-logged in paths
			return Outcome::Forward(Status::Unauthorized);
		}

		//parse to u64
		let mut data = [0u8; 128];
		if hex::decode_to_slice(token.unwrap().value(), &mut data).is_err() {
			return Outcome::Error((Status::Unauthorized, "Could not parse token"));
		}
		let token = SessionToken(data);

		unsafe {
			if !SESSIONS.contains_key(&token) {
				println!("Invalid Token: {:?},", token);
				//remove the invalid token
				req.cookies().remove_private("session-token");
				// We forward so there can be not-logged in paths
				return Outcome::Forward(Status::Unauthorized);
			} else {
				Outcome::Success(Session { token })
			}
		}
	}
}
