use db::create_datatabase_if_not_exists;
use event::{add_event, EventData};
use login::{create_session, LoginData};
use rocket::{
	form::Form,
	fs::NamedFile,
	http::{CookieJar, Status},
	response::{content::RawHtml, Redirect},
};
use session::Session;
use sites::index::create_index_site;
use std::path::{Path, PathBuf};
mod db;
mod elements;
mod event;
mod html;
mod login;
mod session;
mod sites;

#[macro_use]
extern crate rocket;

#[get("/", rank = 1)]
fn index_admin(_s: Session) -> RawHtml<String> {
	rocket::response::content::RawHtml(create_index_site(true).as_string().clone())
}

#[get("/", rank = 2)]
fn index() -> RawHtml<String> {
	rocket::response::content::RawHtml(create_index_site(false).as_string().clone())
}

#[get("/brew-coffee")]
fn brew() -> Status {
	Status::ImATeapot
}

#[get("/<file..>", rank = 99)]
async fn files(file: PathBuf) -> Option<NamedFile> {
	NamedFile::open(Path::new("static/public/").join(file))
		.await
		.ok()
}
#[get("/add-event")]
async fn get_add_event(_s: Session) -> Option<NamedFile> {
	NamedFile::open(Path::new("static/add-event.html"))
		.await
		.ok()
}

#[post("/add-event", data = "<event_data>")]
async fn post_add_event(_s: Session, mut event_data: Form<EventData<'_>>) -> Redirect {
	add_event(&mut event_data).await;
	Redirect::to("/")
}

#[post("/login", data = "<login_data>")]
fn login_fn(login_data: Form<LoginData>, cookies: &CookieJar<'_>) -> Redirect {
	let result = create_session(&login_data);
	if result.is_ok() {
		cookies.add_private(("session-token", hex::encode(result.unwrap())));
		Redirect::to("/add-event")
	} else {
		Redirect::to("/login.html")
	}
}

#[launch]
fn rocket() -> _ {
	create_datatabase_if_not_exists();
	rocket::build().mount(
		"/",
		routes![
			index_admin,
			index,
			files,
			get_add_event,
			login_fn,
			brew,
			post_add_event
		],
	)
}
