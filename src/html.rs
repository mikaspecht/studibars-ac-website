use std::fmt::Display;

pub struct Html(pub String);

impl Html {
	pub fn as_string(&mut self) -> &mut String {
		&mut self.0
	}
}

impl Display for Html {
	fn fmt(&self, f: &mut std::fmt::Formatter<'_>) -> std::fmt::Result {
		f.write_str(self.0.as_str())
	}
}

pub trait HtmlElement {
	fn to_html(&self) -> Html;
}
